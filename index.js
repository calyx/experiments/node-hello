const http = require('http')

function response() {
  return {
    status: "OK",
    date: new Date()
  }
}

const listener = async function(req, res) {
  const body = JSON.stringify(response(), null, 2)
  const headers = {
    "Content-Type": "application/json",
    "Cache-Control": "max-age=30, public"
  }
  res.writeHead(200, headers).end(body)
}

const port = 3000

http.createServer(listener).listen(port)
